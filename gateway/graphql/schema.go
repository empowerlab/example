package graphql

import (
	"fmt"
	"io"
	"log"
	"os"

	"github.com/joho/godotenv"
	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"

	customer "gitlab.com/empowerlab/example/service-customer/gen/graphql"
	customerclient "gitlab.com/empowerlab/example/service-customer/gen/grpc/client"
	invoicing "gitlab.com/empowerlab/example/service-invoicing/gen/graphql"
	invoicingclient "gitlab.com/empowerlab/example/service-invoicing/gen/grpc/client"
	product "gitlab.com/empowerlab/example/service-product/gen/graphql"
	productclient "gitlab.com/empowerlab/example/service-product/gen/grpc/client"
	login "gitlab.com/empowerlab/stack/login/gen/graphql"
	loginclient "gitlab.com/empowerlab/stack/login/gen/grpc/client"
)

var Tracer opentracing.Tracer
var Closer io.Closer

// Resolver todo
type Resolver struct {
	login.ResolverLogin
	customer.ResolverCustomer
	product.ResolverProduct
	invoicing.ResolverInvoicing
}

func init() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	cfg := &jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans:          false,
			CollectorEndpoint: os.Getenv("JAEGER_ENDPOINT"),
		},
	}
	Tracer, Closer, err = cfg.New("gateway", jaegercfg.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	opentracing.SetGlobalTracer(Tracer)

	// Create the client to connect to the targeted service.
	// var err error

	login.GrpcClient, err = loginclient.NewClient(os.Getenv("LOGIN_SERVICE_HOST"), Tracer)
	if err != nil {
		log.Fatal(err, "Couldn't create client")
	}

	customer.GrpcClient, err = customerclient.NewClient(os.Getenv("CUSTOMER_SERVICE_HOST"), Tracer)
	if err != nil {
		log.Fatal(err, "Couldn't create client")
	}

	product.GrpcClient, err = productclient.NewClient(os.Getenv("PRODUCT_SERVICE_HOST"), Tracer)
	if err != nil {
		log.Fatal(err, "Couldn't create client")
	}

	invoicing.GrpcClient, err = invoicingclient.NewClient(os.Getenv("INVOICING_SERVICE_HOST"), Tracer)
	if err != nil {
		log.Fatal(err, "Couldn't create client")
	}

}
