package grpc

import (
	"gitlab.com/empowerlab/stack/lib-go/libgrpc"

	"gitlab.com/empowerlab/example/service-product/orm"
)

var Definitions *libgrpc.Definitions

func init() {

	Definitions = &libgrpc.Definitions{
		Prefix:     "product",
		Repository: "gitlab.com/empowerlab/example/service-product",
	}

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("product"),
	})

}
