package orm

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/drivers/postgres"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	"gitlab.com/empowerlab/stack/lib-go/libdata/specials"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

var Definitions *liborm.Definitions

func init() {

	fmt.Println(os.Getenv("DATABASE_URL"))
	postgresDriver := &postgres.Driver{}
	postgresInfo := &libdata.ClusterInfo{
		Driver: postgresDriver,
		URL:    os.Getenv("DATABASE_URL"),
	}
	var err error
	liborm.Cluster, err = libdata.InitDB(false, postgresInfo, postgresInfo, nil)
	if err != nil {
		log.Fatalf("Error initializing db %s", err.Error())
	}

	Definitions = &liborm.Definitions{
		Repository: "gitlab.com/empowerlab/example/service-customer",
	}

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Cluster: liborm.Cluster,
			Name:    "customer",
			Fields: []libdata.Field{
				&fields.Text{Name: "name", Required: true},
				&fields.Text{Name: "street"},
				&fields.Text{Name: "zip"},
				&fields.Text{Name: "city"},
				&fields.Text{Name: "country"},
			},
			Datetime:    true,
			CanAssignID: false,
		},
	})

	specials.EventModel.Cluster = liborm.Cluster
	libdata.EventDriver = postgresDriver
	Definitions.Register(&liborm.Definition{
		Model: specials.EventModel,
	})
}
