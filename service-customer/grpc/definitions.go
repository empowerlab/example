package grpc

import (
	"gitlab.com/empowerlab/stack/lib-go/libgrpc"

	"gitlab.com/empowerlab/example/service-customer/orm"
)

var Definitions *libgrpc.Definitions

func init() {

	Definitions = &libgrpc.Definitions{
		Prefix:     "product",
		Repository: "gitlab.com/empowerlab/example/service-customer",
	}

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("customer"),
	})

}
