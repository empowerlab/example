import React from 'react';
import PropTypes from 'prop-types';
import { 
  Filter, Responsive, SimpleList, List, Edit, Create, Datagrid, 
  ReferenceField, EditButton, DisabledInput, TextField, LongTextInput, 
  BooleanField, BooleanInput, ReferenceInput, SelectInput, SimpleForm, TextInput, NumberInput,
  NullableBooleanInput, ReferenceManyField,
} from 'react-admin';
// import ActionButton from './mui/Field/ActionButton';
// import TextField from 'admin-on-rest/src/mui/field/TextField';

// const ShopFilter = props => (
//   <Filter {...props}>
//     <TextInput label="Search" source="q" alwaysOn />
//   </Filter>
// );

const ProductFilter = props => (
  <Filter {...props}>
    <TextInput label="ID" source="id" alwaysOn />
  </Filter>
);

export const ProductList = props => (
  <List {...props} filters={<ProductFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

export const ProductCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <NumberInput source="price" />
    </SimpleForm>
  </Create>
);

export const ProductEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <NumberInput source="price" />
    </SimpleForm>
  </Edit>
);
